# Generating a random number and letting user guess the number
# importing built-in random module
import random
my_range = random.randrange(1, 101)  # generates upto 100 but not 101
print(my_range)

my_int = random.randint(1, 100)  # generates upto 100( includes the number we type)
print(my_int)

# ask the user to guess the number.
# Everytime they guess, let they know if they were above or below the number
# So that they can get easier to guess.
user_num = input("Please, type a number: ")
# Making sure that they type the number/given range and not the alphabet or words.
if user_num.isdigit():
    user_num = int(user_num)
    if user_num < -4:
        print("Please type a number greater that it")
        quit()
else:
    print("Please, type a number")
    quit()

# generating random number
random_num = random.randint(-5, user_num)
guess_num = 0
# Keep asking the user to guess until the correct guess.¨
while True:
    guess_num = guess_num+1  # Tracking how many times the user guess to make it correct.
    user_guess = input("Guess the number: ")
    if user_guess.isdigit():
        user_guess = int(user_guess)
    else:
        print("Please, type a number again")
        continue

    if user_guess == random_num:
        print("Congratulations!!! You got it.")
        break
    else:
        print("Sorry. Try again")
        if user_guess > random_num:
            print("You are above the number!!")
        else:
            print("You are below the number!!")

print("You guessed right in", guess_num, "guesses")