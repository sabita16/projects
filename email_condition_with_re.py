# conditions for the email address:
# a-z (lower case)
# 0-9 numbers
# contains only one (.)or (_)
# contains only one @
# . in second or third position
import re
# to start with first letter (^) symbol is used
email_condition = "^[a-z]+[\._]?[a-z 0-9]+[@]\w+[.]\w{2,3}$"
user = input("Enter your email address: ")

# Checking the given email address with the email conditions using regex model search function.
if re.search(email_condition, user):
    print("Thank you. Valid email address")
else:
    print("Sorry. Invalid email address")