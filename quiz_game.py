# Ask the users a bunch of questions.
# If answer the right answer will add 1 point/score to the user
# At the end of the quiz, user are given added total score out of the full score.
# Example. Congratulations on completing the quiz. You score ... points out of ... points.
# First welcoming the user
print("Welcome to the quiz! ")
# Ask if they want to play the quiz
playing = input("Do you want to play? ")
if playing.lower() != "yes":
    print("Thank You. Hope to see you again")
    quit()
else:
    print("Okay. Let's play :-)")
    print("Please, give the right answer of given questions.")
    score = 0

answer = input("Who is father of computer science? ")
if answer.lower() == "charles babbage":
    print("Correct answer")
    score = score+1
else:
    print("Sorry. Incorrect answer")


answer = input("What does CPU stands for? ")
if answer.lower() == "central processing unit":
    print("Correct answer")
    score = score+1
else:
    print("Sorry. Incorrect answer")


answer = input("What does GPU stands for? ")
if answer.lower() == "general processing unit":
    print("Correct answer")
    score = score+1
else:
    print("Sorry. Incorrect answer")


answer = input("What is the name of first computer? ")
if answer.lower() == "atanasoff–berry computer":
    print("Correct answer")
    score = score+1
else:
    print("Sorry. Incorrect answer")


answer = input("Who was the first mother programmer? ")
if answer.lower() == "ada lovelace":
    print("Correct answer")
    score = score+1
else:
    print("Sorry. Incorrect answer")

print("Thank You for answering the questions")

print("You got " + str(score)+" correct answer")
print("You got " + str((score/5)*100)+" % right answer")